var express = require('express'),
app = express(),
neo4j = require('./lib/Neo4jApi.js');
app.use(express.bodyParser());

var TASKS = {
	"task": [
	{
		id: 1,
		name: 'Test',
		startDate: new Date(),
		dueDate: new Date(),
		complete: false,
		appointments: [16,17,18]
	},
	{
		id: 2,
		name: 'Test 2',
		startDate: new Date(),
		dueDate: new Date(),
		complete: false
	},
	{
		id: 3,
		name: 'Test 3',
		startDate: new Date(),
		dueDate: new Date(),
		complete: false,
		appointments: []
	}
	]
};

var APPOINTMENTS = {
	"appointment":	[
	{
		id: 1,
		name: 'Test',
		startDate: new Date(),
		endDate: new Date(),
		task: 1
	},
	{
		id: 2,
		name: 'Test 2',
		startDate: new Date(),
		endDate: new Date(),
		task: 1
	},
	{
		id: 3,
		name: 'Test 3',
		startDate: new Date(),
		endDate: new Date(),
		task: 3
	}
	]
}

app.post('/appointments', function(req,res){
	var appointment = req.body.appointment;
	var deleteQuery = [
	'MATCH (a:Task{id:"' + appointment.task + '"})<-[ns:NoSchedule]-(na:NoAppointment) DELETE ns, na '
	];
	var query = [
	'CREATE (Appointment:Appointment{id:"' + appointment.id + '",',
		'name:"' + appointment.name + '", startDate:"' + appointment.startDate + '",',
		'endDate:"' + appointment.endDate + '"})'

];
if (appointment.task != null)
{
	query = [
	'MATCH (t:Task{id:"' + appointment.task + '"}) ',
	'CREATE (Appointment:Appointment{id:"' + appointment.id + '",',
		'name:"' + appointment.name + '", startDate:"' + appointment.startDate + '",',
		'endDate:"' + appointment.endDate + '"})-[r:Schedule]->(t)'
]
}
neo4j.connect('http://localhost:7474/db/data/', function (err, graph) {
	if (err)
		throw err;
	graph.query(query.join('\n'), {}, function (err, results) {

		if (err) {
			console.log(err);
			console.log(err.stack);
		}
		res.send(results);

	});
});
});

app.put('/appointments/:appointment_id', function(req,res){
	var appointment_id = req.param("appointment_id");
	var appointment = req.body.appointment;
	var query = [
	'MATCH (Appointment:Appointment{id:"' + appointment_id + '"}) ',
	'SET Appointment.name="' + appointment.name + '", Appointment.startDate="' + appointment.startDate + '", ',
	'Appointment.endDate="' + appointment.endDate + '"'
	];

	if (appointment.task != null)
	{
		var deleteRelationship = [
		
		'MATCH (Appointment:Appointment{id:"' + appointment_id + '"})-[r:Schedule]->(t:Task) ',
		'DELETE r '
		
		];

		var createRelationship = [
		'MATCH (task:Task{id:"' + appointment.task + '"}) ',
		'MATCH (apt:Appointment{id:"' + appointment_id + '"}) ',
		'CREATE UNIQUE (apt)-[a:Schedule]->(task)'
		];
	}

	neo4j.connect('http://localhost:7474/db/data/', function (err, graph) {
		if (err)
			throw err;
		graph.query(query.join('\n'), {}, function (err, results) {

			if (err) {
				console.log(err);
				console.log(err.stack);
			}
			if (appointment.task != null)
			{
				graph.query(deleteRelationship.join('\n'), {}, function (err, results) {

					if (err) {
						console.log(err);
						console.log(err.stack);
					}


				});

				graph.query(createRelationship.join('\n'), {}, function (err, results) {

					if (err) {
						res.send(err);
					}
					res.send(results);
				});
			}
			else
			{
				res.send(results);
			}

		});
	});


});

app.put('/tasks/:task_id', function(req,res){
	var task_id = req.param("task_id");
	var task = req.body.task;
	var query = [
	'MATCH (Task:Task{id:"' + task_id + '"}) ',
	'SET Task.name="' + task.name + '", Task.startDate="' + task.startDate + '", ',
	'Task.dueDate="' + task.dueDate + '", Task.complete="' + task.complete + '"'
	];

	neo4j.connect('http://localhost:7474/db/data/', function (err, graph) {
		if (err)
			throw err;
		graph.query(query.join('\n'), {}, function (err, results) {

			if (err) {
				console.log(err);
				console.log(err.stack);
			}
			res.send(results);
		});
	});


});


app.delete('/appointments/:appointment_id', function(req,res){
	var appointment_id = req.param("appointment_id");
	var query = [
	'MATCH (Appointment:Appointment{id:"' + appointment_id + '"})-[r:Schedule]->() ',
	'DELETE r ',
	'UNION MATCH (a:Appointment{id:"' + appointment_id + '"}) ',
	'DELETE a'
	];

	neo4j.connect('http://localhost:7474/db/data/', function (err, graph) {
		if (err)
			throw err;
		graph.query(query.join('\n'), {}, function (err, results) {

			if (err) {
				console.log(err);
				console.log(err.stack);
			}
			res.send(results);
		});
	});


});

app.delete('/tasks/:task_id', function(req,res){
	var task_id = req.param("task_id");
	var query = [
	'MATCH (Task:Task{id:"' + task_id + '"})<-[r:Schedule]-(a:Appointment) ',
	'DELETE r,a ',
	'UNION MATCH (Task:Task{id:"' + task_id + '"}) ',
	'DELETE Task'
	];

	neo4j.connect('http://localhost:7474/db/data/', function (err, graph) {
		if (err)
			throw err;
		graph.query(query.join('\n'), {}, function (err, results) {

			if (err) {
				console.log(err);
				console.log(err.stack);
			}
			res.send(results);
		});
	});


});


app.post('/tasks', function(req,res){
	var task = req.body.task;
	var query = [
	'CREATE (Task:Task{id:"' + task.id + '",',
		'name:"' + task.name + '", startDate:"' + task.startDate + '",',
		'dueDate:"' + task.dueDate + '", complete:"' + task.complete + '"})'
];
neo4j.connect('http://localhost:7474/db/data/', function (err, graph) {
	if (err)
		throw err;
	graph.query(query.join('\n'), {}, function (err, results) {

		if (err) {
			console.log(err);
			console.log(err.stack);
		}
		res.send(results);
	});
});
});



app.get('/appointments', function(req,res){
	
	var query = [
	'MATCH (Appointment:Appointment) WHERE not (Appointment)-[:Schedule]->() ',
	'RETURN Appointment as Appointment, null as Task',
	'UNION MATCH (ap:Appointment)-[s:Schedule]-(t:Task)',
	'RETURN ap as Appointment, t as Task'
	];
	neo4j.connect('http://localhost:7474/db/data/', function (err, graph) {
		if (err)
			throw err;
		graph.query(query.join('\n'), { }, function (err, results) {

			if (err) {
				console.log(err);
				console.log(err.stack);
			}


			var appts = mapToEmberAppointments(query,results);

			

			res.send(appts);
		});
	});
});

app.get('/appointments/:appointment_id', function(req,res){
	var appointment_id = req.param("appointment_id");
	var Appointments = {Appointment: []};
	appts = Appointments;
	var query = [
	'MATCH (Appointment:Appointment{id:"' + appointment_id + '"})',
	'RETURN Appointment'
	];
	neo4j.connect('http://localhost:7474/db/data/', function (err, graph) {
		if (err)
			throw err;
		graph.query(query.join('\n'), {}, function (err, results) {

			if (err) {
				console.log(err);
				console.log(err.stack);
			}
			var appts = mapToEmberAppointments(query,results);
			res.send(appts);
		});
	});
});




app.get('/tasks', function(req, res){
	var query = [
	'MATCH (t:Task)',
	'RETURN t as Task, null as Appointment',
	'UNION MATCH (Task:Task)<-[relation:Schedule]-(Appointment:Appointment)',
	'RETURN Task as Task, Appointment as Appointment'
	];
	neo4j.connect('http://localhost:7474/db/data/', function (err, graph) {
		if (err)
			throw err;
		graph.query(query.join('\n'), { }, function (err, results) {

			if (err) {
				console.log(err);
				console.log(err.stack);
			}
			
			var Tasks = mapToEmberTask(query, results);
			res.send(Tasks);
		});
	});
})

app.get('/tasks/:task_id', function(req,res){
	var task_id = req.param("task_id");
	//var task = req.body.task;
	var query = [
	'MATCH (Task:Task{id:"' + task_id + '"}) ',
	'return Task'
	];

	neo4j.connect('http://localhost:7474/db/data/', function (err, graph) {
		if (err)
			throw err;
		graph.query(query.join('\n'), {}, function (err, results) {

			if (err) {
				console.log(err);
				console.log(err.stack);
			}
			var Tasks = mapToEmberTask(query, results);
			res.send(Tasks);
		});
	});


});

function mapToEmberAppointments(query, results){
	var appts = {Appointment: []};
	if (results != null){
		for (var i = 0; i < results.length; i++) {
			var result = results[i];
			var nodeAppt = result.Appointment;
			var nodeTask = result.Task;
			appts.Appointment[i] = {
				id: nodeAppt.data.id, 
				name: nodeAppt.data.name, 
				startDate: nodeAppt.data.startDate, 
				endDate: nodeAppt.data.endDate
			};
			if (nodeTask != null)
			{
				appts.Appointment[i].task = nodeTask.data.id;
			}
		}
	}
	

	return appts;
}

function mapToEmberTask(query, results){
	var Tasks = {Task: []};
	tasks = Tasks;
	var uniqueTasks = {};
	if (results != null){
		for (var i = 0; i < results.length; i++) {
		var nodeTask = results[i].Task;
		var nodeAppointment = results[i].Appointment;
		var length = Tasks.Task.length;
		for (var j = 0; j < length; j++) {
			var task = Tasks.Task[j];
			if (task.id == nodeTask.data.id)
			{
				for (var k = 0; k < Tasks.Task.length; k++)
				{
					var t = Tasks.Task[k];
					if (t.id == nodeTask.data.id)
					{
						var appointmentNumber = Tasks.Task[k].appointments != null ? Tasks.Task[k].appointments.length : 0
						if (nodeAppointment != null)
						{
							if (appointmentNumber == 0)
							{
								Tasks.Task[k].appointments= [nodeAppointment.data.id];
							}
							else
							{
								Tasks.Task[k].appointments[appointmentNumber] = nodeAppointment.data.id;
							}
						}

						break;
					}
				}
				break;
			}
			var nextIndex = j + 1;
			if (nextIndex == length)
			{
				Tasks.Task[nextIndex] = {
					id: nodeTask.data.id, 
					name: nodeTask.data.name, 
					startDate: nodeTask.data.startDate, 
					dueDate: nodeTask.data.dueDate,
					complete: nodeTask.data.complete
				}
				if (nodeAppointment != null)
				{
					Tasks.Task[nextIndex].appointments = [nodeAppointment.data.id]
				}

				break;
			}

		}
		if (Tasks.Task.length == 0)
		{
			Tasks.Task[0] = {
				id: nodeTask.data.id, 
				name: nodeTask.data.name, 
				startDate: nodeTask.data.startDate, 
				dueDate: nodeTask.data.dueDate,
				complete: nodeTask.data.complete
			}
			if (nodeAppointment != null)
			{
				Tasks.Task[0].appointments = [nodeAppointment.data.id]
			}

		}


	}
	}
	
	return Tasks;
}


app.use(express.static(__dirname + '/public'));

app.listen(3000);
console.log('Listening on port 3000...');