App = Ember.Application.create({
	LOG_TRANSITIONS: true,
	LOG_BINDINGS: true,
	LOG_VIEW_LOOKUPS: true,
	LOG_STACKTRACE_ON_DEPRECATION: true,
	LOG_VERSION: true,
	debugMode: true
});

App.ApplicationAdapter = DS.RESTAdapter.extend({
  host: 'http://122.60.173.209:3000'
});

function generateUUID(){
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x7|0x8)).toString(16);
    });
    return uuid;
};

App.ApplicationRoute = Ember.Route.extend({


  actions: {
    task: function(id){
      this.transitionTo('task', id);
    },
    appointment: function(id){
      this.transitionTo('appointment', id);
    },

    renderTemplate: function() {
      this.render();
      this.render('navigation', {into:'application', outlet: 'navigation'});
    }
  }
});
App.AppointmentController = Ember.ObjectController.extend({
  deleteMode: false,

  actions: {
    edit: function(){
      this.transitionToRoute('appointment.edit');
    },
    delete: function(){

      this.toggleProperty('deleteMode');
    },
    cancelDelete: function(){
      this.set('deleteMode', false);
    },
    confirmDelete: function(){
      var appointment = this.get('model');
      var controller = this;


      appointment.destroyRecord();
      controller.store.dematerializeRecord(appointment);
      appointment.save().then(function(){
        controller.set('deleteMode', false);
        controller.transitionToRoute('appointments');
      });


    }
  }
});
App.AppointmentEditController = Ember.ObjectController.extend({
	task: null,
	activeTasks: function() {
		return this.store.find('task');
	}.property(),
	actions: {

		save: function(){
			var controller = this;
			var appointment = this.get('model');
			this.store.find('task', this.task.id).then(function(task){
				appointment.set('task', task);
				appointment.save().then(function(){
					controller.transitionToRoute('appointment', appointment);
				});
			});
		}
	}
});
App.AppointmentEditRoute = Ember.Route.extend({
	model: function(){
		return this.modelFor('appointment');
	}
});


App.AppointmentRoute = Ember.Route.extend({
	model: function(params) {
		var id = params.appointment_id;
		var recordExists = this.store.hasRecordForId('appointment',id);
		$('.nav-appointments').addClass("active");
		$('.nav-tasks').removeClass("active");
		if (recordExists === true)
		{
			return this.store.find('appointment', params.appointment_id);
		}
		this.transitionTo('appointments');
	},
});
App.AppointmentsCreateController = Ember.ObjectController.extend({
  model:{},
  activeTasks: function() {
    //this.previousTask: this.get('task').get('id');
    return this.store.find('task');
  }.property(),
  actions: {
    save: function(){
      // just before saving, we set the creationDate
      //this.get('model').set('creationDate', new Date());
      // create a record and save it to the store
      var controller = this;
      var appointment = this.get('model');
      appointment.id = generateUUID();
      var newAppointment = this.store.createRecord('appointment', appointment);
      newAppointment.save().then(function(){
        controller.transitionToRoute('appointment', newAppointment);
        newAppointment.reload();
      }, function(err){
        alert(err);
      });

    }
  }
});
App.AppointmentsCreateRoute = Ember.Route.extend({
  model: function(){
    return {appointment: Ember.Object.create({})};;
  },

  renderTemplate: function(){
    this.render('appointment.edit', {
      controller: 'AppointmentsCreate'
    });
  }
});
App.AppointmentsRoute = Ember.Route.extend({
	model: function() {
		$('.nav-appointments').addClass("active");
		$('.nav-tasks').removeClass("active");
		return this.store.find('appointment');
	}
});

App.AuthenticatedRoute = Ember.Route.extend({

  beforeModel: function(transition) {
    if (!this.controllerFor('login').get('token')) {
      this.redirectToLogin(transition);
    }
  },

  redirectToLogin: function(transition) {
    alert('You must log in!');

    var loginController = this.controllerFor('login');
    loginController.set('attemptedTransition', transition);
    this.transitionTo('login');
  },

  actions: {
    error: function(reason, transition) {
      if (reason.status === 401) {
        this.redirectToLogin(transition);
      } else {
        alert('Something went wrong');
      }
    }
  }
});
App.DatetimePickerComponent = Ember.Component.extend({
    pickerWrapper: null,
    pickerData: null,
    classNames: ['input-small'],
    format: "DD/MM/YYYY HH:mm",
    placeholder: Ember.computed.alias('format'),
    iconOnRight: true,

    /**
     * Create function */
    didInsertElement: function () {
        var self = this;
        var format = this.get('format');

        var pickerWrapper = this.$().datetimepicker({
            format: format
        });
        this.set('pickerWrapper', pickerWrapper);

        var pickerData = pickerWrapper.data("DateTimePicker");
        this.set('pickerData', pickerData);
        
        if(this.get('datetime'))
            pickerData.setDate(this.get('datetime'));
        
        pickerWrapper.on('dp.change', function (ev) {
            //self.sendAction('action', ev.format());
            self.set('datetime', ev.date.format(format));
        });

    },

    datetimeChanged: function () {
        // get model datetime
        var modelDatetime = this.get('datetime');
        // get picker datetime
        var picker = this.get('pickerData');
        var pickerDatetime = picker.getDate();
        // if not the same the overwrite model over picker 
        if (modelDatetime != pickerDatetime) {
            picker.setDate(modelDatetime);
        }
    }.observes('datetime')
});
App.IndexRoute = Ember.Route.extend({
	redirect: function(){
		this.transitionTo('appointments');
	}
});
App.LoginController = Ember.Controller.extend({

  reset: function() {
    this.setProperties({
      username: "",
      password: "",
      errorMessage: ""
    });
  },

  token: localStorage.token,
  tokenChanged: function() {
    localStorage.token = this.get('token');
  }.observes('token'),

  login: function() {

    var self = this, data = this.getProperties('username', 'password');

    // Clear out any error messages.
    this.set('errorMessage', null);

    $.post('/auth.json', data).then(function(response) {

      self.set('errorMessage', response.message);
      if (response.success) {
        alert('Login succeeded!');
        self.set('token', response.token);

        var attemptedTransition = self.get('attemptedTransition');
        if (attemptedTransition) {
          attemptedTransition.retry();
          self.set('attemptedTransition', null);
        } else {
          // Redirect to 'articles' by default.
          self.transitionToRoute('articles');
        }
      }
    });
  }
});
App.LoginRoute = Ember.Route.extend({
  setupController: function(controller, context) {
    controller.reset();
  }
});
App.Appointment = DS.Model.extend({
	name: DS.attr('string'),
	startDate: DS.attr('string'),
	endDate: DS.attr('string'),
	task: DS.belongsTo('task', {async:true})
});

App.Appointment.FIXTURES = [
{
	id: 1,
	name: 'Test',
	startDate: new Date(),
	endDate: new Date(),
	task: 1
},
{
	id: 2,
	name: 'Test 2',
	startDate: new Date(),
	endDate: new Date(),
	task: 1
},
{
	id: 3,
	name: 'Test 3',
	startDate: new Date(),
	endDate: new Date(),
	task: 3
}
];
App.Task = DS.Model.extend({
	name: DS.attr('string'),
	startDate: DS.attr('string'),
	dueDate: DS.attr('string'),
	complete: DS.attr('boolean'),
	appointments: DS.hasMany('appointment', {async:true})
});

App.Task.FIXTURES = [
{
	id: 1,
	name: 'Test',
	startDate: new Date(),
	dueDate: new Date(),
	complete: false,
	appointments: [1,2]
},
{
	id: 2,
	name: 'Test 2',
	startDate: new Date(),
	dueDate: new Date(),
	complete: false
},
{
	id: 3,
	name: 'Test 3',
	startDate: new Date(),
	dueDate: new Date(),
	complete: false,
	appointments: [3]
}
];
App.NavigationController = Ember.Controller.extend({
	actions: {
		gotoAppointments: function() {
			this.transitionToRoute('appointments');
		},
		gotoTasks: function() {
			this.transitionToRoute('tasks');
		}
	}
});
App.Router.map(function () {
	this.resource("appointments", function(){
        this.route("create");
    });

    this.resource("appointment", { path: "appointment/:appointment_id"}, function(){
        this.route('edit');
        
    });

    this.resource("tasks", function(){
        this.route("create");
    });

    this.resource("task", { path: "task/:task_id"}, function(){
        this.route('edit');
        
    });

    this.resource('login');
    
});

App.TaskController = Ember.ObjectController.extend({
  deleteMode: false,

  actions: {
    edit: function(){
      this.transitionToRoute('task.edit');
    },
    delete: function(){

      this.toggleProperty('deleteMode');
    },
    cancelDelete: function(){
      this.set('deleteMode', false);
    },
    confirmDelete: function(){
      var task = this.get('model');
      var appointments = task.get("appointments");
      var controller = this;
      appointments.then(function(app){
        app.forEach(function(appointment){
          appointment.deleteRecord();
          controller.store.dematerializeRecord(appointment);
        });
      });
      task.destroyRecord();
      controller.store.dematerializeRecord(task);
      task.save().then(function(){
        controller.set('deleteMode', false);
        controller.transitionToRoute('tasks');
      });
      
    }
  }
});
App.TaskEditController = Ember.ObjectController.extend({
	actions: {
		save: function(){
			var task = this.get('model');
			var controller = this;
			task.save().then(function(){
				controller.transitionToRoute('task', task);
			});

			
		}
		
	}
});
App.TaskEditRoute = Ember.Route.extend({
	model: function(){
		return this.modelFor('task');
	}
});
App.TaskRoute = Ember.Route.extend({
	model: function(params) {
		var id = params.task_id;
		var recordExists = this.store.hasRecordForId('task',id);
		$('.nav-appointments').removeClass("active");
		$('.nav-tasks').addClass("active");
		if (recordExists === true)
		{
			return this.store.find('task', id);
		}
		this.transitionTo('tasks');
		
	}
});
App.TasksCreateController = Ember.ObjectController.extend({
 content:{},
  actions: {
    save: function(){

      var controller = this;
      var task = this.get('model');
      task.id = generateUUID();
      var newTask = this.store.createRecord('task', task);
      newTask.save().then(function(){
      	controller.transitionToRoute('task', newTask);
      	newTask.reload();
      });


    }
  }
});
App.TasksCreateRoute = Ember.Route.extend({
  model: function(){
    return {task: Ember.Object.create({})};
  },

  renderTemplate: function(){
    this.render('task.edit', {
      controller: 'TasksCreate'
    });
  }
});
App.TasksRoute = Ember.Route.extend({
	model: function() {
		$('.nav-tasks').addClass("active");
		$('.nav-appointments').removeClass("active");
		return this.store.find('task');
	}
});