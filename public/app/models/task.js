App.Task = DS.Model.extend({
	name: DS.attr('string'),
	startDate: DS.attr('string'),
	dueDate: DS.attr('string'),
	complete: DS.attr('boolean'),
	appointments: DS.hasMany('appointment', {async:true})
});

App.Task.FIXTURES = [
{
	id: 1,
	name: 'Test',
	startDate: new Date(),
	dueDate: new Date(),
	complete: false,
	appointments: [1,2]
},
{
	id: 2,
	name: 'Test 2',
	startDate: new Date(),
	dueDate: new Date(),
	complete: false
},
{
	id: 3,
	name: 'Test 3',
	startDate: new Date(),
	dueDate: new Date(),
	complete: false,
	appointments: [3]
}
];