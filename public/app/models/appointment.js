App.Appointment = DS.Model.extend({
	name: DS.attr('string'),
	startDate: DS.attr('string'),
	endDate: DS.attr('string'),
	task: DS.belongsTo('task', {async:true})
});

App.Appointment.FIXTURES = [
{
	id: 1,
	name: 'Test',
	startDate: new Date(),
	endDate: new Date(),
	task: 1
},
{
	id: 2,
	name: 'Test 2',
	startDate: new Date(),
	endDate: new Date(),
	task: 1
},
{
	id: 3,
	name: 'Test 3',
	startDate: new Date(),
	endDate: new Date(),
	task: 3
}
];