App.AppointmentsRoute = Ember.Route.extend({
	model: function() {
		$('.nav-appointments').addClass("active");
		$('.nav-tasks').removeClass("active");
		return this.store.find('appointment');
	}
});
