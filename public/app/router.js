App.Router.map(function () {
	this.resource("appointments", function(){
        this.route("create");
    });

    this.resource("appointment", { path: "appointment/:appointment_id"}, function(){
        this.route('edit');
        
    });

    this.resource("tasks", function(){
        this.route("create");
    });

    this.resource("task", { path: "task/:task_id"}, function(){
        this.route('edit');
        
    });

    this.resource('login');
    
});
