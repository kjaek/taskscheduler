App.AppointmentEditController = Ember.ObjectController.extend({
	task: null,
	activeTasks: function() {
		return this.store.find('task');
	}.property(),
	actions: {

		save: function(){
			var controller = this;
			var appointment = this.get('model');
			this.store.find('task', this.task.id).then(function(task){
				appointment.set('task', task);
				appointment.save().then(function(){
					controller.transitionToRoute('appointment', appointment);
				});
			});
		}
	}
});