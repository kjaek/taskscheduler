App.TasksRoute = Ember.Route.extend({
	model: function() {
		$('.nav-tasks').addClass("active");
		$('.nav-appointments').removeClass("active");
		return this.store.find('task');
	}
});