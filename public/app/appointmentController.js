App.AppointmentController = Ember.ObjectController.extend({
  deleteMode: false,

  actions: {
    edit: function(){
      this.transitionToRoute('appointment.edit');
    },
    delete: function(){

      this.toggleProperty('deleteMode');
    },
    cancelDelete: function(){
      this.set('deleteMode', false);
    },
    confirmDelete: function(){
      var appointment = this.get('model');
      var controller = this;


      appointment.destroyRecord();
      controller.store.dematerializeRecord(appointment);
      appointment.save().then(function(){
        controller.set('deleteMode', false);
        controller.transitionToRoute('appointments');
      });


    }
  }
});