App.TasksCreateController = Ember.ObjectController.extend({
 content:{},
  actions: {
    save: function(){

      var controller = this;
      var task = this.get('model');
      task.id = generateUUID();
      var newTask = this.store.createRecord('task', task);
      newTask.save().then(function(){
      	controller.transitionToRoute('task', newTask);
      	newTask.reload();
      });


    }
  }
});