App.TasksCreateRoute = Ember.Route.extend({
  model: function(){
    return {task: Ember.Object.create({})};
  },

  renderTemplate: function(){
    this.render('task.edit', {
      controller: 'TasksCreate'
    });
  }
});