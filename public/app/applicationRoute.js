App.ApplicationRoute = Ember.Route.extend({


  actions: {
    task: function(id){
      this.transitionTo('task', id);
    },
    appointment: function(id){
      this.transitionTo('appointment', id);
    },

    renderTemplate: function() {
      this.render();
      this.render('navigation', {into:'application', outlet: 'navigation'});
    }
  }
});