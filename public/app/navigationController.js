App.NavigationController = Ember.Controller.extend({
	actions: {
		gotoAppointments: function() {
			this.transitionToRoute('appointments');
		},
		gotoTasks: function() {
			this.transitionToRoute('tasks');
		}
	}
});