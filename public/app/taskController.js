App.TaskController = Ember.ObjectController.extend({
  deleteMode: false,

  actions: {
    edit: function(){
      this.transitionToRoute('task.edit');
    },
    delete: function(){

      this.toggleProperty('deleteMode');
    },
    cancelDelete: function(){
      this.set('deleteMode', false);
    },
    confirmDelete: function(){
      var task = this.get('model');
      var appointments = task.get("appointments");
      var controller = this;
      appointments.then(function(app){
        app.forEach(function(appointment){
          appointment.deleteRecord();
          controller.store.dematerializeRecord(appointment);
        });
      });
      task.destroyRecord();
      controller.store.dematerializeRecord(task);
      task.save().then(function(){
        controller.set('deleteMode', false);
        controller.transitionToRoute('tasks');
      });
      
    }
  }
});