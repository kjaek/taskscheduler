App.DatetimePickerComponent = Ember.Component.extend({
    pickerWrapper: null,
    pickerData: null,
    classNames: ['input-small'],
    format: "DD/MM/YYYY HH:mm",
    placeholder: Ember.computed.alias('format'),
    iconOnRight: true,

    /**
     * Create function */
    didInsertElement: function () {
        var self = this;
        var format = this.get('format');

        var pickerWrapper = this.$().datetimepicker({
            format: format
        });
        this.set('pickerWrapper', pickerWrapper);

        var pickerData = pickerWrapper.data("DateTimePicker");
        this.set('pickerData', pickerData);
        
        if(this.get('datetime'))
            pickerData.setDate(this.get('datetime'));
        
        pickerWrapper.on('dp.change', function (ev) {
            //self.sendAction('action', ev.format());
            self.set('datetime', ev.date.format(format));
        });

    },

    datetimeChanged: function () {
        // get model datetime
        var modelDatetime = this.get('datetime');
        // get picker datetime
        var picker = this.get('pickerData');
        var pickerDatetime = picker.getDate();
        // if not the same the overwrite model over picker 
        if (modelDatetime != pickerDatetime) {
            picker.setDate(modelDatetime);
        }
    }.observes('datetime')
});