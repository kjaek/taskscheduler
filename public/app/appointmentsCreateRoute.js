App.AppointmentsCreateRoute = Ember.Route.extend({
  model: function(){
    return {appointment: Ember.Object.create({})};;
  },

  renderTemplate: function(){
    this.render('appointment.edit', {
      controller: 'AppointmentsCreate'
    });
  }
});