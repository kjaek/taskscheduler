App.TaskEditController = Ember.ObjectController.extend({
	actions: {
		save: function(){
			var task = this.get('model');
			var controller = this;
			task.save().then(function(){
				controller.transitionToRoute('task', task);
			});

			
		}
		
	}
});