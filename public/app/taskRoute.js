App.TaskRoute = Ember.Route.extend({
	model: function(params) {
		var id = params.task_id;
		var recordExists = this.store.hasRecordForId('task',id);
		$('.nav-appointments').removeClass("active");
		$('.nav-tasks').addClass("active");
		if (recordExists === true)
		{
			return this.store.find('task', id);
		}
		this.transitionTo('tasks');
		
	}
});