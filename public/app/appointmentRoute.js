App.AppointmentRoute = Ember.Route.extend({
	model: function(params) {
		var id = params.appointment_id;
		var recordExists = this.store.hasRecordForId('appointment',id);
		$('.nav-appointments').addClass("active");
		$('.nav-tasks').removeClass("active");
		if (recordExists === true)
		{
			return this.store.find('appointment', params.appointment_id);
		}
		this.transitionTo('appointments');
	},
});