App.AppointmentsCreateController = Ember.ObjectController.extend({
  model:{},
  activeTasks: function() {
    //this.previousTask: this.get('task').get('id');
    return this.store.find('task');
  }.property(),
  actions: {
    save: function(){
      // just before saving, we set the creationDate
      //this.get('model').set('creationDate', new Date());
      // create a record and save it to the store
      var controller = this;
      var appointment = this.get('model');
      appointment.id = generateUUID();
      var newAppointment = this.store.createRecord('appointment', appointment);
      newAppointment.save().then(function(){
        controller.transitionToRoute('appointment', newAppointment);
        newAppointment.reload();
      }, function(err){
        alert(err);
      });

    }
  }
});